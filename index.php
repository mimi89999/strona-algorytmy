<?php
require_once 'Twig/autoload.php';

include 'config.php';

//ini_set('session.cookie_secure', '1');
//ini_set('session.cookie_httponly', '1');

//session_start();

$db = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);

$loader = new Twig_Loader_Filesystem('templates');
$twig = new Twig_Environment($loader, array('cache' => 'compilation_cache',));

$statement = $db->prepare('SELECT name, title FROM pages ORDER BY kolejnosc');
$exec = $statement->execute();
$pages = $statement->fetchAll(PDO::FETCH_ASSOC);

$requested_page = str_replace('/pages/', '', $_SERVER['REQUEST_URI']);

if ($requested_page == '/') {
    $requested_page = '';
}

$statement = $db->prepare('SELECT title, content FROM pages WHERE name = ?');
$exec = $statement->execute([$requested_page]);
$page_data = $statement->fetchAll(PDO::FETCH_ASSOC);

$main_template = $twig->load('index.html');
echo $main_template->render(array('pages' => $pages,
 'page_data' => $page_data[0],
 'authenticated' => True));

?>