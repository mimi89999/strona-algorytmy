﻿<!DOCTYPE HTML>

<html>
<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <link rel="stylesheet" href="/res/w3.css">
  <title>Coś o algorytmach</title>
</head>

<body class="w3-sand">

<form class="w3-container w3-card-4 w3-display-middle">

<p>
<label class="w3-text"><b>First Name</b></label>
<input class="w3-input w3-border" type="text">
</p>

<p>
<label class="w3-text"><b>Last Name</b></label>
<input class="w3-input w3-border" type="text">
</p>

<p>
<button class="w3-btn w3-blue">Login</button>
</p>

</form>

</body>
</html>